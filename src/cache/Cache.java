/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cache;

import java.util.HashMap;

/**
 *
 * @author Sanda
 */
public class Cache extends HashMap<String, Object> {
    private static Cache INSTANCE;
    
    private Cache(){}
    
    public static Cache getInstance()
    {		
        if (INSTANCE == null)
        {
            INSTANCE = new Cache();	
        }
        return INSTANCE;
    }
    
    public static void destroy(){
        Cache cache = Cache.getInstance();
        cache.clear();
    }
}
