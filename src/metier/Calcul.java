/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import java.sql.Date;

/**
 *
 * @author Sanda
 */
public class Calcul {
    private Date daty;
    private CalculDetail client;
    private CalculDetail bilan;

    public Calcul(Date daty)throws java.lang.Exception{
        this.setDaty(daty);

    }
    public CalculDetail getClient() {
        return client;
    }

    public void setClient(CalculDetail client,String titre) {
        this.client = client;
        this.client.setTitre(titre);
    }    
    public void setClient(CalculDetail client) {
        this.setClient(client, "Client");
    }

    public CalculDetail getBilan() {
        return bilan;
    }

    public void setBilan(CalculDetail bilan,String titre) {
        this.bilan = bilan;
        this.bilan.setTitre(titre);
    }
    public void setBilan(CalculDetail bilan) {
        this.setBilan(bilan, "Bilan");
    }
    
    public Integer getMontant(){
        Integer montant = 0,bilan = 0 , client = 0;
        if(this.getBilan() != null) bilan = this.getBilan().getMontant();
        if(this.getClient() != null) client = this.getClient().getMontant();
        montant = bilan - client ;
        return montant;
    }
    public String getMontantMGA(){
        return Montant.montantMGA(this.getMontant());
    }
    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty)throws java.lang.Exception {
        if(daty == null) this.daty = Service.dateDuJour();
        else this.daty = daty;
    }
    @Override
    public String toString(){
        String versionString = "";
        versionString += "Date: "+this.getDaty()+"\n";
        versionString+= ""+this.getClient()+"\n";
        versionString+= ""+this.getBilan()+"\n";
        versionString+= "Montant: "+Montant.integerToString(this.getMontant())+"\n";
        return versionString;
    }
}
