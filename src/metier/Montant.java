/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

/**
 *
 * @author Sanda
 */
public class Montant {
    private Integer value = 0;
    
    private Montant(Integer value){
        this.setValue(value);
    }
    private Montant(String value) throws Exception{
        this.setValue(value);
    }

    private void setValue(Integer value) {
        this.value = value;
    }
    private void setValue(String value) throws Exception {
        String montantSansEspace = enleverEspace(value);
        if(montantSansEspace.contains(".") || montantSansEspace.contains(",")){
            throw new Exception("Aucun monnaie ne contient de virgule");
        }
        Service.controlChiffre(montantSansEspace);
        Integer integerValue = Integer.valueOf(montantSansEspace);
        this.setValue(integerValue);
    }
    /**
     * Access private becaus any extern fonction must not call this method
     * @return Integer
     */
    private Integer getValue() {
        return value;
    }
    
    private static String enleverEspace(String montant){
        String newValue = "";
        int taille = montant.length();
        String temp;
        for(int i=0; i<taille; i++){
            temp = ""+montant.charAt(i);
            if(temp.equals(" ") == false)   newValue += montant.charAt(i);
        }
        return newValue;
    }
    /**
     * Afin de rendre un chiffre plus lisible
     * @return 
     */
    private String getFormater(){
        String montant = "";
        String entier = String.valueOf(this.getValue()),
                entierInverse = "";
        int taille = entier.length(), count = 0, i = 0;
        for(i= taille - 1; i>=0; i--){
            entierInverse += entier.charAt(i);
            count++;
            if(count == 3){
                entierInverse += " ";
                count = 0;
            }
        }
        montant += Service.inverse(entierInverse);
        
        return montant;
    }
    /**
     * pas fait
     * @return 
     */
    private String getLettre(){
        return ChiffreLettre.convertIntToString(this.getValue())+" "+Constante.uniteAriary;
    }
    /**
     * format value to Integer
     * @param montant
     * @return 
     * @throws java.lang.Exception 
     */
    public static Integer stringToInteger(String montant) throws Exception{
        Montant valueMontant = new Montant(montant);
        return valueMontant.getValue();
    }
    
    /**
     * 
     * @param montant
     * @return String
     */
    public static String integerToString(Integer montant){
        Montant valueMontant = new Montant(montant);
        return valueMontant.getFormater();
    }
    /**
     * return price into Ariary (MGA : Malagasy Ariary)
     * @param montant
     * @return String
     */
    public static String montantMGA(Integer montant){
        Montant valueMontant = new Montant(montant);
        return valueMontant.getLettre();
    }
}
